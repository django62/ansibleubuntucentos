#!/usr/bin/bash


sed -i '/OUTPUT ACCEPT/ a -A INPUT -p tcp -m multiport --dports 20,21,60000:65535 -j ACCEPT' /etc/sysconfig/iptables
echo "UseIPv6 off" >> /etc/proftpd.conf
echo "IdentLookups off" >> /etc/proftpd.conf
echo "PassivePorts 60000 65535" >> /etc/proftpd.conf
